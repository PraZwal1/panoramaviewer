const panoImage = document.querySelector('.pano-image');
const img = '/static/images/Image.jpg';


const panorama = new PANOLENS.ImagePanorama(img);
const viewer = new PANOLENS.Viewer({
    container: panoImage,
    controlBar: false
});

viewer.add( panorama )